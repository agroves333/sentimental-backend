import falcon
import random
import json
from wsgiref import simple_server

class TwitterResource(object):
    def on_get(self, req, resp):
        result = json.dumps([
            {
                'sentiment': random.uniform(-1, 1),
                'magnitude': random.uniform(0, 10)
            },
            {
                'sentiment': random.uniform(-1, 1),
                'magnitude': random.uniform(0, 10)
            },
            {
                'sentiment': random.uniform(-1, 1),
                'magnitude': random.uniform(0, 10)
            }
        ])
        """Handles GET requests"""
        resp.status = falcon.HTTP_200
        resp.set_header('Access-Control-Allow-Origin', '*')
        resp.body = (result)

# falcon.API instances are callable WSGI apps
app = falcon.API()

# Resources are represented by long-lived class instances
tweets = TwitterResource()

app.add_route('/api/search', tweets)




server = simple_server.make_server('127.0.0.1', 8000, app)
server.serve_forever()
